package ru.gerasimova.tm.constant;

public class DataConstant {

    public static final String FILE_BINARY = "./data.bin";

    public static final String FILE64_BINARY = "./data64.bin";

    public static final String FILE_XML = "./data.xml";

    public static final String FILE_JSON = "./data.json";

}
