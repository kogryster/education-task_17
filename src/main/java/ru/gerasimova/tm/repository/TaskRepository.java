package ru.gerasimova.tm.repository;

import ru.gerasimova.tm.api.repository.ITaskRepository;
import ru.gerasimova.tm.entity.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

//  private final Map<String, Task> map = new LinkedHashMap<>();


    @Override   // Для map - мне не нужно, у меня есть add
    public Task createTask(final String name) {
        final Task task = new Task();
        task.setName(name);
        merge(task);
        return task;
    }
/* ВОПРОС
    @Override // Для map
    public Task getTaskById(final String id) {
        if (id == null || id.isEmpty()) return null;
        //return  map.get(id);
        return tasks.get(id);
    }

 */
/*
    @Override  // Для map
    public Task getByOrderIndex(Integer orderIndex) {
        if (orderIndex == null) return null;

        return getListTask.get(orderIndex);
    }

 */
/*   ВОПРОС

    @Override  // не Для map ???
    public Task getByOrderIndex(Integer orderIndex) {
        if (orderIndex == null) return null;
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            result.add(task);
        }
        return result;
    }
    */


    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (userId.equals(task.getUserId()))
            tasks.remove(task);
        else return;
    }

    @Override
    public List<Task> findAll() {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            result.add(task);
        }
        return result;
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Task> tasks = findAll(userId);
        this.tasks.removeAll(tasks);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        for (final Task task : tasks) {
            boolean idMatch = false;
            boolean userIdMatch = false;
            if (id.equals(task.getId())) idMatch = true;
            if (userId.equals(task.getUserId())) userIdMatch = true;
            if (idMatch && userIdMatch) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (final Task task : tasks) {
            boolean nameMatch = false;
            boolean userIdMatch = false;
            if (name.equals(task.getName())) nameMatch = true;
            if (userId.equals(task.getUserId())) userIdMatch = true;
            if (nameMatch && userIdMatch) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        remove(userId, task);
        return task;
    }

    @Override  // V new
    public void load(final Collection<Task> tasks) {
        clear();
        merge(tasks);
    }

    /*
    @Override  //new
    public void load(final Task... tasks) {
        clear();// что передать?
        merge(tasks);
    }

     */


    @Override  //new
    public void merge(final Task... tasks) {
        for (final Task task : tasks) merge(task); // так ли?

    }

    @Override  // V new групповая вставка
    public void merge(final Collection<Task> tasks) {
        for (final Task task : tasks) merge(task);

    }

    @Override  // V new единичная вставка
    public Task merge(Task task) {
        if (task == null) return null;
        tasks.add(task);
        //tasks.add(task.getId(), task);
        //tasks.put(task.getId(), task);
        //в видео map.put(task.getId(), task);
        return task;
    }

    @Override //new
    public void removeTaskById(final String id) {
        if (id == null || id.isEmpty()) return;
        //map.remove(id);
        tasks.remove(id);
    }

   /*  есть аналог removeOneByIndex ?
    @Override //new
    public void removeTaskByOrderIndex(final Integer orderIndex) {
        Task task = getByOrderIndex(orderIndex);
        if (task == null) return;
        removeTaskById(task.getId());
    }

    */

    @Override  // V new метод очистки всего , не связанный с пользоватеме
    public void clear() {
        //map.clear();
        tasks.clear();
    }
}
