package ru.gerasimova.tm.service;

import ru.gerasimova.tm.api.repository.ICommandRepository;
import ru.gerasimova.tm.api.service.ICommandService;
import ru.gerasimova.tm.dto.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArguments() {
        return commandRepository.getArguments();
    }
}
