package ru.gerasimova.tm.command.data.binary;

import ru.gerasimova.tm.command.AbstractCommand;
import ru.gerasimova.tm.constant.DataConstant;
import ru.gerasimova.tm.dto.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-bin-load";
    }

    @Override
    public String description() {
        return "Load data from binary file. (Load).";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();

        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());

        serviceLocator.getAuthService().logout();
        System.out.println("[LOGOUT OK]");

        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");
    }

}
