package ru.gerasimova.tm.command.data.binary;

import ru.gerasimova.tm.command.AbstractCommand;
import ru.gerasimova.tm.constant.DataConstant;

import java.io.File;
import java.nio.file.Files;

public final class DataBinaryClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-bin-clear";
    }

    @Override
    public String description() {
        return "Remove binary data. (Clear).";
    }

    @Override
    public void execute() throws Exception {
        final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
        System.out.println();
    }

}
