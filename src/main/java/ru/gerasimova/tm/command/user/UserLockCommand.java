package ru.gerasimova.tm.command.user;


import ru.gerasimova.tm.command.AbstractCommand;
import ru.gerasimova.tm.enumeration.Role;
import ru.gerasimova.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "lock-user";
    }

    @Override
    public String description() {
        return "Lock user";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
