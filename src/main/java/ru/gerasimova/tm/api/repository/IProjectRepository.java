package ru.gerasimova.tm.api.repository;

import ru.gerasimova.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository {
    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll();

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneById(String userId, String id);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneById(String userId, String id);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

    void load(final Collection<Project> projects);

    //  void load(final Project... projects);

    void merge(final Project... projects);

    void merge(final Collection<Project> projects);

    Project merge(Project project);

    void clear();

}
