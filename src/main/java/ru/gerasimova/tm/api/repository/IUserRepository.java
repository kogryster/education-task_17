package ru.gerasimova.tm.api.repository;

import ru.gerasimova.tm.entity.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User updateUserPasswordHash(String id, String passwordHash);

    void load(final Collection<User> users);

    //  void load(final User... users);

    void merge(final User... users);

    void merge(final Collection<User> users);

    User merge(User user);

    void clear();
}
