package ru.gerasimova.tm.api.repository;

import ru.gerasimova.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    void add(String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findAll();

    List<Task> findAll(String userId);

    void clear(String userId);

    Task findOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneById(String userId, String id);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneByName(String userId, String name);

    void load(Collection<Task> tasks);

    // void load (Task... tasks);

    void merge(Task... tasks);

    void merge(Collection<Task> tasks);

    Task merge(Task task);

    Task createTask(String name);

    //Task getTaskById(String id);

    //  Task getByOrderIndex(Integer orderIndex);

    void removeTaskById(String id);

    // void removeTaskByOrderIndex(Integer orderIndex);

    void clear();

}
