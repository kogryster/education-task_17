package ru.gerasimova.tm.exception.empty;

public class NullUserException extends AbstractEmptyException {

    public NullUserException() {
        super("Error! system returned \"null\" current user...");
    }

}
