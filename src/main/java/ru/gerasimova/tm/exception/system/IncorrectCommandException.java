package ru.gerasimova.tm.exception.system;

public class IncorrectCommandException extends AbstractSystemException {

    public IncorrectCommandException(String value) {
        super("Error! Сommand ``" + value + "``not supported...");
    }

}
