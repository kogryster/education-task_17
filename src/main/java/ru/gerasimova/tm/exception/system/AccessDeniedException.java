package ru.gerasimova.tm.exception.system;

public class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }
}
